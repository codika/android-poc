package com.emiliano.mallo.poc.model;

import com.parse.ParseObject;
import com.emiliano.mallo.poc.util.ListFactory;

import java.util.List;

/**
 * Created by Emiliano Mallo
 */
public class HotelTransformer {
    public static final String HOTEL_ENTITY_API_NAME="Hotel";
    private static final String HOTEL_NAME="name";
    private static final String HOTEL_DISTANCE="distance";
    private static final String HOTEL_PRICE="price";
    private static final String HOTEL_STAR_RATING="starRating";
    private static final String HOTEL_IMAGE_URL="imageUrl";
    private static final String HOTEL_DETAIL="detail";


    public List<Hotel> getHotelsFromApiModel(List<ParseObject> apiObjects){
        List<Hotel> hotels=new ListFactory().createEmptyArrayList();
        for(ParseObject parseObject:apiObjects){
            hotels.add(getHotelFromApiModel(parseObject));
        }
        return hotels;
    }

    private Hotel getHotelFromApiModel(ParseObject object){
        Hotel hotel=new Hotel();
        hotel.setName(object.getString(HOTEL_NAME));
        hotel.setDetail(object.getString(HOTEL_DETAIL));
        hotel.setDistance(object.getNumber(HOTEL_DISTANCE).floatValue());
        hotel.setPrice(object.getNumber(HOTEL_PRICE).floatValue());
        hotel.setStarRating(object.getNumber(HOTEL_STAR_RATING).intValue());
        hotel.setImageUrl(object.getString(HOTEL_IMAGE_URL));
        return hotel;
    }
}
