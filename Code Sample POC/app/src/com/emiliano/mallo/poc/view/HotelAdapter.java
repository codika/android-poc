package com.emiliano.mallo.poc.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.emiliano.mallo.poc.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.emiliano.mallo.poc.model.Hotel;

import java.util.List;

/**
 * Created by emi91_000 on 09/02/2015.
 */
public class HotelAdapter extends BaseRecycleViewAdapter<HotelViewHolder,Hotel> {
    private Context context;

    public HotelAdapter(List<Hotel> hotels, Context context) {
        this.itemList = hotels; this.context=context;
    }

    @Override
    public HotelViewHolder onCreateViewHolder(ViewGroup parent,
                                              int viewType) {
        // create a new view
        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hotel, parent, false);
        HotelViewHolder vh = new HotelViewHolder(v);
        return vh;
    }

    @Override
    public void onBindBaseViewHolder(HotelViewHolder holder, int position, Hotel item) {
        holder.name.setText(item.getName());
        holder.distance.setText(context.getString(R.string.distance, Float.toString(item.getDistance())));
        holder.price.setText(context.getString(R.string.price_with_currency, item.getPrice()));
        holder.starRating.setRating(item.getStarRating());
        holder.description.setText(item.getDetail());
        ImageLoader.getInstance().displayImage(item.getImageUrl(), holder.image);
    }
}