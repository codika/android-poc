package com.emiliano.mallo.poc.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.emiliano.mallo.poc.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.emiliano.mallo.poc.model.Hotel;
import com.emiliano.mallo.poc.model.HotelTransformer;

import java.util.List;

public class POCActivity extends Activity {
    private RecyclerView recyclerView;
    private BaseRecycleViewAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private LoadingLayout loadingLayout;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
        loadingLayout= (LoadingLayout) findViewById(R.id.loading_container);
        initRecycleView();


        ParseQuery<ParseObject> query = ParseQuery.getQuery(HotelTransformer.HOTEL_ENTITY_API_NAME);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if(e!=null){
                    Toast.makeText(POCActivity.this,R.string.server_error,Toast.LENGTH_SHORT).show();
                }else{
                    adapter=new HotelAdapter(new HotelTransformer().getHotelsFromApiModel(parseObjects),POCActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.setOnItemClickListener(new BaseRecycleViewAdapter.OnItemClickListener<Hotel>() {
                        @Override
                        public void onItemClickListener(Hotel hotel, int position) {
                            Toast.makeText(POCActivity.this,getString(R.string.hotel_selected,hotel.getName()),Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                loadingLayout.stopLoading();
            }
        });
	}

    private void initRecycleView() {
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }
}
