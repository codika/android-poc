package com.emiliano.mallo.poc.view;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.emiliano.mallo.poc.R;
/**
 * Created by Emiliano Mallo
 */
public class HotelViewHolder extends BaseRecycleViewAdapter.BaseViewHolder {
    public TextView price;
    public TextView name;
    public TextView description;
    public TextView distance;
    public ImageView image;
    public ViewGroup container;
    public RatingBar starRating;

    public HotelViewHolder(ViewGroup viewGroup) {
        super(viewGroup);
        this.price = (TextView) viewGroup.findViewById(R.id.price);
        this.description = (TextView) viewGroup.findViewById(R.id.detail);
        this.distance = (TextView) viewGroup.findViewById(R.id.distance);
        this.name = (TextView) viewGroup.findViewById(R.id.title);
        this.image = (ImageView) viewGroup.findViewById(R.id.hotel_image);
        container = (ViewGroup) viewGroup.findViewById(R.id.list_item_container);
        this.starRating = (RatingBar) viewGroup.findViewById(R.id.rating_bar);
    }
}