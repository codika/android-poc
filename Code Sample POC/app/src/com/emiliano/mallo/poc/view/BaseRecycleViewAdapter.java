package com.emiliano.mallo.poc.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * Created by emi91_000 on 09/03/2015.
 */
public abstract class BaseRecycleViewAdapter<T extends BaseViewHolder,E> extends RecyclerView.Adapter {
    protected OnItemClickListener<E> onItemClickListener;
    protected List<E> itemList;

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        onBindBaseViewHolder((T) holder, position, itemList.get(position));
        ((T) holder).setOnClickListener(new BaseViewHolder.OnViewHolderClickListener() {
            @Override
            public void onViewHolderClickListener(BaseViewHolder holder) {
                onItemClickListener.onItemClickListener(itemList.get(position),position);
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener<E> {
        public void onItemClickListener(E object, int position);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public abstract void onBindBaseViewHolder(T holder, int position,E item);
}
