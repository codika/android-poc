package com.emiliano.mallo.poc.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by emi91_000 on 11/03/2015.
 */
public class BaseViewHolder extends RecyclerView.ViewHolder{
    public OnViewHolderClickListener onClickListener;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public void setOnClickListener(final OnViewHolderClickListener onClickListener) {
        this.onClickListener = onClickListener;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickListener!=null)
                    onClickListener.onViewHolderClickListener(BaseViewHolder.this);
            }
        });
    }

    public interface OnViewHolderClickListener{
        public void onViewHolderClickListener(BaseViewHolder holder);
    }
}


