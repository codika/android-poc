package com.emiliano.mallo.poc.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emiliano Mallo
 */
public class ListFactory {
    public <T> List<T> newArrayList(T item){
        List<T> arrayList=new ArrayList<T>();
        arrayList.add(item);
        return arrayList;
    }

    public List createEmptyArrayList() {
        return new ArrayList();
    }
}
