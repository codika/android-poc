#This is a small example of items list.#
* It uses Universal Image Loader for caching images.
* I used Parse.com like API hosting.
* LoadingLayout is a Custom View that is used to wrap some views that needs to be replaced with a progress.
* ListFactory is an util class where i use generics like an example of uncoupling of List interface and it's implementations.
* Recycle view (from Android Materials) and i also created a BaseRecycleViewAdapter who adds with generics the chance of add onItemClickListener.
* Selectors to give a better look and feel to the items.
* HotelTransformer is responsible of uncoupling api model and android application model.